
public class SquareList {
	public static class IntList {
		public int head;
		public IntList tail;

		public IntList(int h, IntList t) {
		    head = h;
		    tail = t;
		}
		public String toString() {
		    if (tail == null)
		        return Integer.toString(head);
		    return Integer.toString(head) + " " + tail.toString();
    	}
	}
	/** Destructively squares each element of the given IntList L.
	* Don’t use ’new’; modify the original IntList.
	* Should be written iteratively. */
	public static IntList SquareDestructive(IntList L) {
		IntList p = L;
		while (p!=null) {
			p.head *= p.head;
			p = p.tail;
		}
		return L;
	}
	/** Non-destructively squares each element of the given IntList L.
	* Don’t modify the given IntList.
	* Should be written recursively*/
	public static IntList SquareNonDestructive(IntList L) {
		if ( L == null) return null;
		// recursion
//		IntList p = new IntList(L.head * L.head, null);
//		p.tail = SquareNonDestructive(L.tail);
//		return p;
		
		// iteration
		IntList B,LSquare;
		B = new IntList(L.head * L.head, null);
		LSquare = B;
		while (L!= null) {
			B.tail = new IntList(L.head*L.head,null);
			B = B.tail;
			L = L.tail;
		}
		return LSquare;
	}
	public static void main(String[] args) {
        IntList L = new IntList(5, null);
        L.tail = new IntList(7, null);
        L.tail.tail = new IntList(9, null);
        System.out.println(SquareDestructive(L));
        System.out.println(L);
        
        System.out.println(SquareNonDestructive(L));
        System.out.println(L);
		System.out.println("hello world!");
	}
}

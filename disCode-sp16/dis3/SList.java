/* Represent a list of integers, where all the "list" work is delegated
 * to a naked IntList. */
/**
 * @author Yucheng Wu
 *
 */

import org.omg.PortableInterceptor.INACTIVE;

public class SList {
	public class IntNode {
		public int item;     /* Equivalent of first */
		public IntNode next; /* Equivalent of rest */

		public IntNode(int i, IntNode h) {
			item = i;
			next = h;
		}
	} 

	private IntNode sentinel;
	private int size;
	/** Creates an empty list. */
	public SList() {
		// 此处要给sentinel初始化分配空间，否则有空指针错误
		sentinel = new IntNode(0, null);
		size = 0;
	}

	public SList(int x) {
		sentinel = new IntNode(0, null);
		sentinel.next = new IntNode(x, null);
		size = 1;
	}

	/** Adds an item of the front. */
	public void insertFront(int x) {
		IntNode p = new IntNode(x, null);
		p.next = sentinel.next;
		sentinel.next = p;
		size += 1;
	}

	/** Gets the front item of the list. */
	public int getFront() {
		return sentinel.next.item;
	}

	/** Puts an item at the back of the list. */
	public void insertBack(int x) {
		IntNode p = sentinel;
		while (p.next != null) {
			p = p.next;
		}
		p.next = new IntNode(x, null);
		size += 1;
	}

	/** Returns the back node of our list. */
	private IntNode getBackNode() {
		IntNode p = sentinel;
		while ( p.next != null) {
			p = p.next;
		}
		return p;
	}

	/** Returns last item */
	public int getBack() {
		IntNode p = getBackNode();
		return p.item;
	}

	public int size() {
		return size;
	}

	/**
	 * Add a method to the SList class that inserts a new element at the given position.
	 * If the position is past the end of the list, insert the new node at the end of the list.
	 * For example, if the SList is 5 –> 6 –> 2,
	 * insert(10, 1) should result in 5 –> 10 –> 6 –> 2.
	 * @param item
	 * @param position
	 */
	public void insert(int item, int position) {
		if (position < 1) {
			System.out.println("the value of position must greater than 1!");
		} else {
			IntNode p = sentinel,
					tp= new IntNode(item, null);
			while (position > 0 && p.next != null) {
				p = p.next;
				position--;
			}
			tp.next = p.next;
			p.next	= tp;
			size += 1;
		}
	}

	public void print() {
		IntNode p = sentinel.next;
		for (int i = 0; i < size; i++) {
			System.out.print(p.item + " ");
			p = p.next;
		}
		System.out.println();
	}

	/**
	 * Add another method to the SList class that reverses the elements.
	 * Do this using the existing SNodes (you should not use new).
	 * discussion 3, reverse
	 */
	public void reverseIteration() {
		// iteratively
		if (size > 1) {
			IntNode pre, pnxt, tmp;
			pre = sentinel.next;
			pnxt = sentinel.next.next;
			tmp = null;
			while (pnxt.next != null) {
				// reverse point
				tmp = pnxt.next;
				pnxt.next = pre;
				// two pointers move on
				pre = pnxt;
				pnxt = tmp;
			}
			pnxt.next = pre;
			sentinel.next = pnxt;
		}
	}

	public IntNode reverseRecuHelper(IntNode p) {

		if (p.next == null) {
			return p;
		}

		IntNode pnx;
		pnx = reverseRecuHelper(p.next);
		p.next.next = p;
		p.next = null;
		return pnx;
	}
	public void reverseRecursion() {
		// recursion
		if (size > 1) {
			sentinel.next = reverseRecuHelper(sentinel.next);
		}
	}

	public static void main(String[] args) {
		SList s1 = new SList(11);
		s1.insertBack(6);
		s1.insertFront(4);
		s1.insertFront(3);
		System.out.println(s1.getBack());
		System.out.println(s1.getFront());

		/**
		 * insert test
		 */
//		s1.print();
//		s1.insert(1, 5);
//		s1.print();
//		s1.insert(4, 8);
//		s1.print();
//		s1.insert(6, 2);
//		s1.print();
//		s1.insert(9, 0);
//		s1.print();

		/**
		 * test reverse
		 */
//
		System.out.println("reverseRecursion");
		s1.print();
		s1.reverseRecursion();
		s1.print();
		System.out.println("reverseIteration");
		s1.print();
		s1.reverseIteration();
		s1.print();
	}
} 
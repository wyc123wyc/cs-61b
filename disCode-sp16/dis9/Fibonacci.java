import java.util.*;

public class Fibonacci {
	HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();

	// For your reference, fib(0) = 0, fib(1) = 1, fib(2) = 1, ...
	public int fib(int n) {
		if (n == 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		} else if (map.containsKey(n)) {
			return map.get(n);
		}

		map.put(0,0);
		map.put(1,1);
		for (int i = 2; i <= n; i++) {
			map.put(i,map.get(i - 1) + map.get(i - 2));
		}
		return map.get(n);
	}

	public static void main(String[] args) {
		Fibonacci fibc = new Fibonacci();
		System.out.println(fibc.fib(9));
		System.out.println(fibc.fib(8));
		System.out.println(fibc.fib(10));
	}
}
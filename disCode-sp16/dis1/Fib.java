public class Fib {
	public static void main(String[] args) {
		for (int i=0;i<14;i++)
		System.out.print(fib(i)+" ");
		
	}
	// my solution
	public static int my_fib(int N) {
		if (N == 0) return -1;
		if (N == 1) return 0;
		if (N == 2) return 1; 
		int p1 = 1;
		int p2 = 0;
		int ans=0;
		while (N > 2) {
			N--;
			ans = p1 + p2;
			p2 = p1;
			p1 = ans;
			
		}
		return ans;
	}
	// offical 
	/** fib(N) returns the Nth Fibonacci number, for N ≥ 0.
	* The Fibonacci sequence is 0, 1, 1, 2, 3, 5, 8, 13, 21, ... */
	public static int fib(int N) {
		if (N <= 1) {
			return N;
		} else {
			return fib(N - 1) + fib(N - 2);
		}
	}
	// We can also write this iteratively:
	/** fib(N) returns the Nth Fibonacci number, for N ≥ 0.
	* The Fibonacci sequence is 0, 1, 1, 2, 3, 5, 8, 13, 21, ... */
//	public static int fib(int N) {
//		int f0 = 0;
//		int f1 = 1;
//		while (N > 0) {
//			int temp = f1;
//			f1 = f0 + f1;
//			f0 = temp;
//			N -= 1;
//		}
//		return f0;
//	}

	//Extra for experts: Complete fib2 in 5 lines or less. Your answer must be efficient.
	public static int fib2(int n, int k, int f0, int f1) {
		if (n == k) {
			return f0;
		} else {
			return fib2(n, k + 1, f1, f0 + f1);
		}
	}
	//To compute the Nth fibonacci number using fib2, call fib2(N, 0, 0, 1).
}

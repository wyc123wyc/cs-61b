/** Array based list.
 *  @author Josh Hug
 */


/**
 * @author Yucheng Wu
 */
public class AList {
    /** Creates an empty list. */
    private int size;
    private int[] items;
    private static int REFACTOR = 2;
    public AList() {
        items = new int[100];
        size = 0;
    }

    /** Inserts X into the back of the list. */
    public void insertBack(int x) {
        if (size == items.length) {
            int[] tmp = new int[REFACTOR * items.length];
            System.arraycopy(items, 0, tmp, 0, items.length);
            items = tmp;
        }
        items[size] = x;
        size += 1;
    }

    /** Returns the item from the back of the list. */
    public int getBack() {
        return items[size - 1];
    }
    /** Gets the ith item in the list (0 is the front). */
    public int get(int i) {
        return items[i];
    }

    /** Deletes item from back of the list and
      * returns deleted item. */
    public int deleteBack() {
        int tmp = items[size] = 0;
        size -= 1;
        return tmp;
    }

    /** Returns the number of items in the list. */
    public int size() {
        return size;
    }
} 
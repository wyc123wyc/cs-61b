/** Fill in the map implementation below. */
public class ArrayMap<K, V> {
	private K[] keys;
	private V[] values;
	private int size;

	public ArrayMap() {
		keys = (K[]) new Object[100];
		values = (V[]) new Object[100];
	}

	/**
	 * Finds a given key and return its index
	 * or -1 if not in list.
	 * @param key
	 * @return
	 */
	private int findKey(K key) {
		for (int i = 0; i < size; i++) {
			if (keys[i].equals(key)) {
				return i;
			}
		}
		return -1;
	}

	public void put(K key, V value) {
		int loc = findKey(key);
		if (loc < 0) {
			keys[size] = key;
			values[size] = value;
		} else {
			values[loc] = value;
		}
		size += 1;
	}

	public V get(K key) {				
		return values[findKey(key)];
	}

	public boolean containsKey(K key) {					
		int i = findKey(key);
		return i > -1;
	}

	public K[] keys() {
		return keys;
	}
} 
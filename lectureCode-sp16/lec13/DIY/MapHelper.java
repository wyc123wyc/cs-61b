import static org.junit.Assert.*;

public class MapHelper {

	/* Add a method get, which returns an item if it exists
	 * and null otherwise. */
	public  static <K, V> V get(ArrayMap<K, V> am, K key) {
		if (am.containsKey(key)) {
			return am.get(key);
		}
		return null;
	}

	/* Add a method maxKey, which returns the maximum key. */
	public  static <K, V> K maxKey(ArrayMap<K, V> am) {
		K[] keys = am.keys();
		K maxkey = keys[0];
//		for (int i = 0; i < keys.length; i++) {
//			if (keys[i] > maxkey) {
//				maxkey = keys[i];
//			}
//		}
//		return maxkey;
		return null;
	}


	public static void main(String[] args) {
  		ArrayMap<Integer, String> am = new ArrayMap<Integer, String>();
  		am.put(5, "hello");
  		am.put(10, "ketchup");

  		assertEquals("hello", MapHelper.get(am, 5));
  		assertEquals(null, MapHelper.get(am, 99));


//  		assertEquals(5, MapHelper.maxKey(am));
	}
} 
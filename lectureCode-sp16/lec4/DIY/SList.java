/* Represent a list of integers, where all the "list" work is delegated
 * to a naked IntList. */

import com.sun.org.apache.xpath.internal.functions.FuncFalse;
import org.omg.PortableInterceptor.INACTIVE;

/**
 * @author Yucheng Wu
 */

public class SList {
	public class IntNode {
		public int item;     /* Equivalent of first */
		public IntNode next; /* Equivalent of rest */

		public IntNode(int i, IntNode h) {
			item = i;
			next = h;
		}
	} 

	private IntNode sentinel;
	private int size;
	/** Creates an empty list. */
	public SList() {
		// 此处要给sentinel初始化分配空间，否则有空指针错误
		sentinel = new IntNode(0, null);
		size = 0;
	}

	public SList(int x) {
		sentinel = new IntNode(0, null);
		sentinel.next = new IntNode(x, null);
		size = 1;
	}

	/** Adds an item of the front. */
	public void insertFront(int x) {
		IntNode p = new IntNode(x, null);
		p.next = sentinel.next;
		sentinel.next = p;
		size += 1;
	}

	/** Gets the front item of the list. */
	public int getFront() {
		return sentinel.next.item;
	}

	/** Puts an item at the back of the list. */
	public void insertBack(int x) {
		IntNode p = sentinel;
		while (p.next != null) {
			p = p.next;
		}
		p.next = new IntNode(x, null);
		size += 1;
	}

	/** Returns the back node of our list. */
	private IntNode getBackNode() {
		IntNode p = sentinel;
		while ( p.next != null) {
			p = p.next;
		}
		return p;
	}

	/** Returns last item */
	public int getBack() {
		IntNode p = getBackNode();
		return p.item;
	}

	public int size() {
		return size;
	}

	public static void main(String[] args) {
		SList s1 = new SList(11);
		s1.insertBack(6);
		s1.insertFront(4);
		s1.insertFront(3);
		System.out.println(s1.getBack());
		System.out.println(s1.getFront());
		System.out.println(s1.size());
	}
} 
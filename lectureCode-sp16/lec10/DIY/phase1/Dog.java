public class Dog {
    private String name;
    private int size;

    public Dog(String n, int s) {
        name = n;
        size = s;
    }

    public void bark() {
        System.out.println(name + " says: bark");
    }
}

 class ShowDog extends Dog {

	 public ShowDog (String n, int s) {
		 super(n, s);
	 }

	@Override
	public void bark() {
		System.out.println("says: bark show!");
	}

	public static void main(String[] args) {
		Object o2 = new ShowDog("Corgi", 10);

		ShowDog sdx = ((ShowDog)o2);
		sdx.bark();

		Dog dx = ((Dog)o2);
		dx.bark();

		((Dog)o2).bark();

		Object o3 = (Dog)o2;
		//o3.bark();
	}
}

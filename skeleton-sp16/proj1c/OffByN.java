/**
 * Created by wyc on 16-4-10.
 *
 *  implement the CharacterComparator interface, as well as a single argument constructor
 *  which takes an integer. In other words, the callable methods and constructors will be:

 OffByN(int N)
 equalChars(char x, char y)

 * The OffBYN constructor should return an object whose equalChars method returns true for
 * characters that are off by N. For example the call to equal chars below should return true,
 * since a and f are off by 5 letters.

 OffByN offby5 = new OffByN(5);
 offBy5.equalChars('a', 'f')
 */
public class OffByN implements CharacterComparator {

	private int N;
	public OffByN(int N) {
		this.N = N;
	}

	@Override
	public boolean equalChars(char x, char y) {
		int nx = Character.getNumericValue(x);
		int ny = Character.getNumericValue(y);
		if (Math.abs(nx - ny) == N) {
			return true;
		}
		return false;
	}

}

/**
 * Created by wyc on 16-4-10.
 */
public class Palindrome	 {

	/**
	 * The wordToDeque method should be straightforward. You will simply build a Deque
	 * where the characters in the deque appear in the same order as in the word.
	 * @param word
	 * @return
	 */
	public static Deque<Character> wordToDeque(String word) {
		Deque<Character> w = new LinkedListDeque<Character>();
		for (int i = 0; i < word.length(); i++) {
			w.addLast(word.charAt(i));
		}
		return w;
	}

	/**
	 * The isPalindrome(回文数) method should return true if the given word is a palindrome,
	 * and false otherwise.
	 * @param word
	 * @return
	 */
	public static boolean isPalHelper(int front, int rear, String s) {
		if (front >= rear) {
			return true;
		}
		if (s.charAt(front) == s.charAt(rear)) {
			return isPalHelper(front + 1, rear - 1, s);
		}
		return false;
	}
	public static boolean isPalindrome(String word) {

		// Deque<Character> w = wordToDeque(word);
		// w.getRecursive()
		return isPalHelper(0, word.length()-1, word);
	}

	/**
	 * The method will return true if the word is a palindrome according to the character comparison test
	 * provided by the CharacterComparator passed in as argument cc.
	 * A character comparator is defined as shown above:
	 * @param word
	 * @param cc
	 * @return
	 */
	public static boolean isPalindromeHelper(int front, int rear, String w, CharacterComparator cc) {
		if (front >= rear) {
			return true;
		}
		if (cc.equalChars(w.charAt(front), w.charAt(rear))) {
			return isPalindromeHelper(front + 1, rear - 1, w, cc);
		}
		return false;
	}
	public static boolean isPalindrome(String word, CharacterComparator cc) {
		return isPalindromeHelper(0, word.length() - 1, word, cc);
	}
}

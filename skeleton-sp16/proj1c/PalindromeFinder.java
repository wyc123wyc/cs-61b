/** This class outputs all palindromes in the words file in the current directory. */
public class PalindromeFinder {
    public static void main(String[] args) {
        int minLength = 4;
        In in = new In("words.txt");
//        System.out.println("print palindrome :");
//        while (!in.isEmpty()) {
//            String word = in.readString();
//            if (word.length() >= minLength && Palindrome.isPalindrome(word, new CharacterComparator() {
//                @Override
//                public boolean equalChars(char x, char y) {
//                    if (x == y) {
//                        return true;
//                    }
//                    return false;
//                }
//            })) {
//                System.out.println(word);
//            }
//        }

//        in = new In("words.txt");
//        OffByOne obo = new OffByOne();
//        System.out.printf("print off by one words:");
//        while (!in.isEmpty()) {
//            String word = in.readString();
//            if (word.length() >= minLength && Palindrome.isPalindrome(word, obo)) {
//                System.out.println(word);
//            }
//        }

        in = new In("words.txt");
        OffByN obn = new OffByN(4);
        System.out.printf("print off by N words:");
        while (!in.isEmpty()) {
            String word = in.readString();
            if (word.length() >= minLength && Palindrome.isPalindrome(word, obn)) {
                System.out.println(word);
            }
        }
    }
} 

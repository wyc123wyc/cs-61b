/**
 *
 * Created by wyc on 16-4-10.
 *
 * Create an interface in Deque.java that contains all of the methods that appear in
 * both ArrayDeque and LinkedListDeque. See the project 1a spec for a concise list.
 *
 * modify any Deque implementation you intend to use for later parts of this project
 * (LinkedListDeque, ArrayDeque, or LinkedListDequeSolution) so that they implement the
 * Deque interface. Add @Override tags to each method that overrides a Deque method.
 */
public interface Deque<Item> {

	// constant declarations, if any

	// method signatures
	// using file LinkedListDeque.java in proj1
	/**
	 * Adds an item to the front/back of the Deque.
	 * @link http://blog.csdn.net/cyp331203/article/details/42388065
	 * @param item
	 */
	void addFirst(Item item);
	void addLast(Item item);
	/**
	 * Returns true if deque is empty, false otherwise.
	 * @return
	 */
	boolean isEmpty();
	/**
	 * Returns the number of items in the Deque.
	 * @return
	 */
	int size();
	/**
	 * Prints the items in the Deque from first to last, separated by a space.
	 */
	void printDeque();
	/**
	 * Removes and returns the item at the front of the Deque.
	 * If no such item exists, returns null.
	 * @return
	 */
	Item removeFirst();
	/**
	 * Removes and returns the item at the back of the Deque.
	 * If no such item exists, returns null.
	 * @return
	 */
	public Item removeLast();
	/**
	 * Gets the item at the given index, where 0 is the front,
	 * 1 is the next item, and so forth.
	 * If no such item exists, returns null. Must not alter the deque!
	 * @param index
	 * @return
	 */
	Item get(int index);
	/**
	 * Same as get, but uses recursion.
	 * @param index
	 * @return
	 */
	Item getRecursive(int index);

}

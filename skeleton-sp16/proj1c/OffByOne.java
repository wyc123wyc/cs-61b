/**
 * Created by wyc on 16-4-10.
 *
 * mplement CharacterComparator such that equalChars returns true for letters
 * that are different by one letter. For example the following calls to obo should return true.
 * Note that characters are delineated in Java by single quotes, in contrast to Strings,
 * which use double quotes.

 OffByOne obo = new OffByOne();
 obo.equalChars('a', 'b')
 obo.equalChars('r', 'q')
 However, the two calls below should return false:

 obo.equalChars('a', 'e')
 obo.equalChars('z', 'a')

 * A palindrome is a word that is the same when read forwards and backwards. To allow for
 * odd length palindromes, we do not check the middle character for equality with itself.
 * So "flake" is an off-by-1 palindrome, even though 'a' is not one character off from itself.
 */
public class OffByOne implements CharacterComparator{
	@Override
	public boolean equalChars(char x, char y) {
		int nx = Character.getNumericValue(x);
		int ny = Character.getNumericValue(y);
		if (Math.abs(nx - ny) == 1) {
			return true;
		}
		return false;
	}
}

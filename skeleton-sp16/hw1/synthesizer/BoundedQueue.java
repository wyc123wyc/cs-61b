package synthesizer;

/**
 * @Task_1 : BoundedQueue
 * The BoundedQueue is similar to our Deque from project 1, but with a more limited API. Specifically,
 * items can only be enqueued at the back of the queue, and can only be dequeued from the front of the queue.
 * Unlike our Deque, the BoundedDeque has a fixed capacity, and nothing is allowed to enqueue if the Deque is full.
 *
 * Created by wyc on 16-4-14.
 */

/**
 * All method declarations in an interface, including default methods, are implicitly public,
 * so you can omit the public modifier.
 * @param <T>
 */
public interface BoundedQueue<T> extends Iterable<T> {
	int capacity();          // return size of the buffer
	int fillCount();         // return number of items currently in the buffer
	void enqueue(T x);  // add item x to the end
	T dequeue();        // delete and return item from the front
	T peek();           // return (but do not delete) item from the front

	/**
	 * return true if queue is empty, otherwise false.
	 * @return
	 */
	default boolean isEmpty() {
		return fillCount() == 0;
	}       // is the buffer empty (fillCount equals zero)?

	/**
	 * return true if queue is full, otherwise false.
	 * @return
	 */
	default boolean isFull()  {
		return fillCount() == capacity();
	}       // is the buffer full (fillCount is same as capacity)?
}

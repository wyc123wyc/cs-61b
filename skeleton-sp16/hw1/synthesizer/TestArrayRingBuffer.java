package synthesizer;
//import examples.In;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.*;

/** Tests the ArrayRingBuffer class.
 *  @author Josh Hug
 */

public class TestArrayRingBuffer {

	/* Utility method for printing out empty checks. */
	public static boolean checkEmpty(boolean expected, boolean actual) {
		if (expected != actual) {
			System.out.println("isEmpty() returned " + actual + ", but expected: " + expected);
			return false;
		}
		return true;
	}


	/* Utility method for printing out empty checks. */
	public static boolean checkSize(int expected, int actual) {
		if (expected != actual) {
			System.out.println("size() returned " + actual + ", but expected: " + expected);
			return false;
		}
		return true;
	}

	/* Prints a nice message based on whether a test passed.
	 * The \n means newline. */
	public static void printTestStatus(boolean passed) {
		if (passed) {
			System.out.println("Test passed!\n");
		} else {
			System.out.println("Test failed!\n");
		}
	}

	@Test
    public void someTest() {
        ArrayRingBuffer<Integer> arb = new ArrayRingBuffer(3);
        boolean passed = checkEmpty(true, arb.isEmpty());

		passed = checkSize(0, arb.fillCount) && passed;

		arb.enqueue(1);
		passed = checkEmpty(false, arb.isEmpty());
		arb.enqueue(2);
		arb.enqueue(3);

		passed = checkSize(3, arb.fillCount()) && passed;

//		for (int i = 0; i < arb.capacity; i++) {
//			arb.dequeue();
//		}
//
//		passed = checkSize(0, arb.fillCount) && passed;
//		passed = checkEmpty(true, arb.isEmpty()) && passed;

		printTestStatus(passed);
//		arb.dequeue();
		Iterator<Integer> seer = arb.iterator();
		while (seer.hasNext()) {
			System.out.println(seer.next());
		}

		for (Integer x : arb) {
			System.out.printf(x.toString() + " ");
		}
    }

    /** Calls tests for ArrayRingBuffer. */
    public static void main(String[] args) {
        jh61b.junit.textui.runClasses(TestArrayRingBuffer.class);
    }
} 

package synthesizer;

/**
 * Created by wyc on 16-4-14.
 */
public abstract class AbstractBoundedQueue<T> implements BoundedQueue<T>{
	protected int fillCount;
	protected int capacity;
	public int capacity() {
		return capacity;
	}
	public int fillCount() {
		return fillCount;
	}
	/**
	 * Note that isEmpty, isFull, peek, dequeue, enqueue, are inherited from BoundedQueue,
	 * so you should not to declare these explicitly in your AbstractBoundedQueue.java file.
	 */
//	public boolean isEmpty()
//	public boolean isFull()
//	public abstract T peek();
//	public abstract T dequeue();
//	public abstract void enqueue(T x);
}

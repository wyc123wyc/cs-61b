package lab8;

import com.sun.org.apache.xml.internal.dtm.ref.EmptyIterator;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by wyc on 16-7-26.
 */
public class BSTMap<K extends Comparable<K>, V> implements Map61B<K, V> {
	private Node root;             // root of BST

	private class Node {
		private K key;           // sorted by key
		private V val;         // associated data
		private Node left, right;  // left and right subtrees
		private int size;          // number of nodes in subtree

		public Node(K key, V val, int size) {
			this.key = key;
			this.val = val;
			this.size = size;
		}
	}

	/**
	 * Initializes an empty symbol table.
	 */
	public BSTMap() {}

	/** Removes all of the mappings from this map. */
	public void clear(){
		if(root != null) {
			root = null;
		}
	}

	/* Returns true if this map contains a mapping for the specified key. */
	public boolean containsKey(K key){
		if (key == null) throw new NullPointerException("argument to contains() is null");
		return get(key) != null;
	}

	/* Returns the value to which the specified key is mapped, or null if this
	 * map contains no mapping for the key.
	 */
	public V get(K key) {
		return get(root, key);
	}

	private V get(Node x, K key) {
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if      (cmp < 0) return get(x.left, key);
		else if (cmp > 0) return get(x.right, key);
		else              return x.val;
	}

	/* Returns the number of key-value mappings in this map. */
	public int size(){
		return size(root);
	}

	private int size(Node x) {
		if (x == null) return 0;
		else return x.size;
	}

	/* Associates the specified value with the specified key in this map. */
	public void put(K key, V value){
		if (key == null) throw new NullPointerException("first argument to put() is null");
		if (value == null) {
			remove(key);
			return;
		}
		root = put(root, key, value);
	}

	private Node put(Node x, K key, V val) {
		if (x == null) return new Node(key, val, 1);
		int cmp = key.compareTo(x.key);
		if      (cmp < 0) x.left  = put(x.left,  key, val);
		else if (cmp > 0) x.right = put(x.right, key, val);
		else              x.val   = val;
		x.size = 1 + size(x.left) + size(x.right);
		return x;
	}

	/* Returns a Set view of the keys contained in this map. */
	public Set<K> keySet(){
		try {
			throw new UnsupportedOperationException("Unsupported Operation Exception.");
		} catch (java.lang.UnsupportedOperationException e) {
			System.out.println("Invalid operation for remove. ");
		}
		return null;
	}

	public Iterator<K> iterator() {
		try {
			throw new UnsupportedOperationException("Unsupported Operation Exception.");
		} catch (java.lang.UnsupportedOperationException e) {
			System.out.println("Invalid operation for remove. ");
		}
		Iterator<K> key = new Iterator<K>() {
			@Override
			public boolean hasNext() {
				return false;
			}

			@Override
			public K next() {
				return null;
			}
		};
		return key;
	}

	/* Removes the mapping for the specified key from this map if present.
	 * Not required for Lab 8. If you don't implement this, throw an
	 * UnsupportedOperationException. */
	public  V remove(K key){
		try {
			throw new UnsupportedOperationException("Unsupported Operation Exception.");
		} catch (java.lang.UnsupportedOperationException e) {
			System.out.println("Invalid operation for remove. ");
		}
		return null;
	}

	/* Removes the entry for the specified key only if it is currently mapped to
	 * the specified value. Not required for Lab 8. If you don't implement this,
	 * throw an UnsupportedOperationException.*/
	public V remove(K key, V value){
		try {
			throw new UnsupportedOperationException("Unsupported Operation Exception.");
		} catch (java.lang.UnsupportedOperationException e) {
			System.out.println("Invalid operation for remove. ");
		}
		return null;
	}
}

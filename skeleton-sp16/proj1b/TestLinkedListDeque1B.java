import org.junit.Test;

/**
 * http://stackoverflow.com/a/25200968
 * To make CrazyCoder comment more visible, if you go for the "proceed on errors" behavior
 * you may want to uncheck
 * ==Settings | Compiler | Automatically show first error in editor.==
 *
 * Created by wyc on 16-4-9.
 *
 * You should start by making sure you can write code that uses the StudentArrayDeque and
 * StudentLinkedListDeque classes.
 * An example is provided in the examples/StudentArrayDequeLauncher.java file.
 *
 * In TestArrayDeque1B.java, you should give a JUnit test that StudentArrayDeque fails
 * Likewise in TestLinkedListDeque1B.java, you should give a JUnit test that StudentLinkedListDeque fails.
 *
 * Important: When building tests, you must select Integer as your test type, StudentArrayDeque<Integer>
 */
public class TestLinkedListDeque1B {

	/* Utility method for printing out empty checks. */
	public static boolean checkEmpty(boolean expected, boolean actual) {
		if (expected != actual) {
			System.out.println("isEmpty() returned " + actual + ", but expected: " + expected);
			return false;
		}
		return true;
	}

	/* Utility method for printing out empty checks. */
	public static boolean checkSize(int expected, int actual) {
		if (expected != actual) {
			System.out.println("size() returned " + actual + ", but expected: " + expected);
			return false;
		}
		return true;
	}

	/* Prints a nice message based on whether a test passed.
	 * The \n means newline. */
	public static void printTestStatus(boolean passed) {
		if (passed) {
			System.out.println("Test passed!\n");
		} else {
			System.out.println("Test failed!\n");
		}
	}

	public static boolean checkItem(int expected, int actual) {
		if (expected != actual) {
			System.out.println("get()/getRecursive() returned " + actual + ", but expected: " + expected);
			return false;
		}
		return true;
	}

	@Test
	public void addRemoveIsEmptySizeTest() {
		StudentLinkedListDeque<Integer> sll = new StudentLinkedListDeque<>();

		System.out.println("Running add/isEmpty/Size test.");

		boolean passed = checkEmpty(true, sll.isEmpty());
		passed = checkSize(0, sll.size()) && passed;
		passed = checkEmpty(true, sll.isEmpty()) && passed;

		sll.removeFirst();
		sll.removeLast();
		passed = checkEmpty(true, sll.isEmpty()) && passed;
		passed = checkSize(0, sll.size()) && passed;

		sll.addFirst(1);
		passed = checkEmpty(false, sll.isEmpty()) && passed;
		passed = checkSize(1, sll.size()) && passed;
		sll.removeLast();
		passed = checkEmpty(true, sll.isEmpty()) && passed;

		for (int i = 0; i < 8; i++) {
			sll.addLast(i + 1);
		}
		passed = checkSize(8, sll.size()) && passed;
		sll.addLast(9);
		passed = checkSize(9, sll.size()) && passed;
		sll.printDeque();
		System.out.println();

		for (int i = 0; i < 7; i++) {
			sll.removeLast();
		}
		passed = checkSize(2, sll.size()) && passed;
		passed = checkItem(1, sll.getRecursive(0)) && passed;
		passed = checkItem(2, sll.get(1)) && passed;/** bug, get() function does't judge the parameter's range! */

		printTestStatus(passed);
	}
}

import org.junit.Assert;
import org.junit.Test;
/**
 * Created by wyc on 16-4-9.
 */
public class TestArrayDeque1B {

	/* Utility method for printing out empty checks. */
	public static boolean checkEmpty(boolean expected, boolean actual) {
		if (expected != actual) {
			System.out.println("isEmpty() returned " + actual + ", but expected: " + expected);
			return false;
		}
		return true;
	}


	/* Utility method for printing out empty checks. */
	public static boolean checkSize(int expected, int actual) {
		if (expected != actual) {
			System.out.println("size() returned " + actual + ", but expected: " + expected);
			return false;
		}
		return true;
	}

	/* Prints a nice message based on whether a test passed.
	 * The \n means newline. */
	public static void printTestStatus(boolean passed) {
		if (passed) {
			System.out.println("Test passed!\n");
		} else {
			System.out.println("Test failed!\n");
		}
	}

	@Test
	public void addIsEmptySizeTest() {
		StudentArrayDeque<Integer> sad = new StudentArrayDeque<>();
		System.out.println("Running add/isEmpty/Size test.");

		boolean passed = checkEmpty(true, sad.isEmpty());
		passed = checkSize(0, sad.size()) && passed;
		passed = checkEmpty(true, sad.isEmpty()) && passed;

		sad.removeFirst();
		passed = checkEmpty(true ,sad.isEmpty()) && passed;
		passed = checkSize(0, sad.size()) && passed;/** bug , size() return -1 */

//		sad.addFirst(1);
//		passed = checkSize(1, sad.size()) && passed;
//		passed = checkEmpty(false, sad.isEmpty()) && passed;
//
//		sad.addLast(2);
//		passed = checkSize(2, sad.size()) && passed;
//		passed = checkEmpty(false, sad.isEmpty()) && passed;
//
//		for (int i = 0; i < 5; i++) {
//			sad.addLast(i + 3);
//		}
//		passed = checkSize(7, sad.size()) && passed;
//		sad.addFirst(-1);
//		passed = checkSize(8, sad.size()) && passed;
//		passed = checkEmpty(false, sad.isEmpty()) && passed;
//
//		sad.removeFirst();
//		passed = checkSize(7, sad.size()) && passed;
//		sad.removeLast();
//		passed = checkSize(6, sad.size()) && passed;

		printTestStatus(passed);
	}
}

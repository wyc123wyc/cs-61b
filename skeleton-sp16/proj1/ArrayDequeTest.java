import org.junit.Test;

/**
 * Created by wyc on 16-4-4.
 */
public class ArrayDequeTest {

	/* Utility method for printing out empty checks. */
	public static boolean checkEmpty(boolean expected, boolean actual) {
		if (expected != actual) {
			System.out.println("isEmpty() returned " + actual + ", but expected: " + expected);
			return false;
		}
		return true;
	}

	/* Utility method for printing out empty checks. */
	public static boolean checkSize(int expected, int actual) {
		if (expected != actual) {
			System.out.println("size() returned " + actual + ", but expected: " + expected);
			return false;
		}
		return true;
	}

	/* Prints a nice message based on whether a test passed.
	 * The \n means newline. */
	public static void printTestStatus(boolean passed) {
		if (passed) {
			System.out.println("Test passed!\n");
		} else {
			System.out.println("Test failed!\n");
		}
	}

	/** Adds a few things to the list, checking isEmpty() and size() are correct,
	 * finally printing the results.
	 *
	 * && is the "and" operation. */
	@Test
	public void addIsEmptySizeTest() {
		System.out.println("Running add/isEmpty/Size test.");
		System.out.println("Make sure to uncomment the lines below (and delete this print statement).");

		ArrayDeque<Integer> arr = new ArrayDeque<>();

		boolean passed = checkEmpty(true, arr.isEmpty());

		arr.addFirst(3);
		passed = checkSize(1, arr.size()) && passed;
		passed = checkEmpty(false, arr.isEmpty()) && passed;
		for (int i = 0; i < 10; i++) {
			arr.addLast(i);
		}
		passed = checkSize(11, arr.size()) && passed;

		System.out.println("Printing out deque: ");
		arr.printDeque();


		for (int i = 0; i < 100; i++) {
			arr.addFirst(i*4);
		}
		passed = checkSize(111, arr.size()) && passed;
		arr.printDeque();
		printTestStatus(passed);
	}

	@Test
	/** Adds an item, then removes an item, and ensures that dll is empty afterwards. */
	public void addRemoveTest() {

		System.out.println("Running add/remove test.");

		System.out.println("Make sure to uncomment the lines below (and delete this print statement).");

		ArrayDeque<Integer> arr = new ArrayDeque<>();
		// should be empty
		boolean passed = checkEmpty(true, arr.isEmpty());

		for (int i = 0; i < 10; i++) {
			arr.addLast(i);
		}
		arr.addFirst(90);
		// should not be empty
		passed = checkEmpty(false, arr.isEmpty()) && passed;
		passed = checkSize(11, arr.size()) && passed;

		int i = arr.size();
		while(!arr.isEmpty()) arr.removeFirst();
		// should be empty
		passed = checkEmpty(true, arr.isEmpty()) && passed;

		printTestStatus(passed);

	}
}

/**
 * @author wyc
 * Created by wyc on 16-4-3.
 *
 * This Deque must use arrays as the core data structure.
 * Operations are subject to the following rules:
 * @rule1 add and remove must take constant time, except during resizing operations.
 * @rule2 get and size must take constant time.
 * @rule3 The starting size of your array should be 8.
 * @rule4 The amount of memory that your program uses at any given time must be
 * proportional to the number of items. For example, if you add 10,000 items to the Deque,
 * and then remove 9,999 items, you shouldn't still be using an array of length 10,000ish.
 * For arrays of length 16 or more, your usage factor should always be at least 25%.
 * For smaller arrays, your usage factor can be arbitrarily low.
 */

public class ArrayDeque<Item> {
	//private int size;
	private Item[] items;
	// usage factor >= 25%, so REFACTOR should less than 4
	private static int REFACTOR = 2;
	private int maxSize = 8;
	// front points to the first element
	// rear points to the next position of the last element.
	private int front, rear;


	/** Creates an empty deque. */
	public ArrayDeque() {
		// TODO finished: ArrayDeque
		front = rear = 0;
		items = (Item[]) new Object[maxSize];
	}

	private void resize(int capacity) {
		// TODO finished: resize
		Item[] tmp = (Item[]) new Object[capacity];
		int i;
		for (i = 0; front != rear; i++) {
			tmp[i] = items[front];
			front = (front + 1) % maxSize;
		}
		items = tmp;
		front = 0;
		rear = i;
		maxSize = capacity;
	}
	/**
	 * Adds an item to the front/back of the Deque.
	 * @param item
	 */
	public void addFirst(Item item) {
		// TODO finished: addFirst
		if (isFull()) {
			resize(size() * REFACTOR);
		}
		front = (front - 1 + maxSize) % maxSize;
		items[front] = item;
	}
	public void addLast(Item item) {
		// TODO finished: addLast
		if (isFull()) {
			resize(size() * REFACTOR);
		}
		items[rear] = item;
		rear = (rear + 1) % maxSize;
	}

	/**
	 * Returns true if deque is full, false otherwise.
	 * @return
	 */
	private boolean isFull() {
		// TODO finished: isFull
		return (rear + 1) % maxSize == front;
	}

	/**
	 * Returns true if deque is empty, false otherwise.
	 * @return
	 */
	public boolean isEmpty() {
		// TODO finished: isEmpty
		return front == rear;
	}

	/**
	 * Returns the number of items in the Deque.
	 * @return
	 */
	public int size() {
		// TODO finished: size
		return (rear - front + maxSize ) % maxSize;
	}

	/**
	 * Prints the items in the Deque from first to last, separated by a space.
	 */
	public void printDeque() {
		// TODO finished: printDeque
		int i = front;
		while (i != rear) {
			System.out.print(items[i] + " ");
			i = (i + 1) % maxSize;
		}
		System.out.println();
	}

	/**
	 * Removes and returns the item at the front of the Deque.
	 * If no such item exists, returns null.
	 * @return
	 */
	public Item removeFirst() {
		// TODO finished: removeFirst
		if (isEmpty()) {
			return null;
		}
		Item p = items[front];
		front = (front + 1) % maxSize;
		return p;
	}

	/**
	 * Removes and returns the item at the back of the Deque.
	 * If no such item exists, returns null.
	 * @return
	 */
	public Item removeLast() {
		// TODO finished: removeLast
		if (isEmpty()) {
			return null;
		}
		rear = (rear - 1 + maxSize) % maxSize;
		Item p = items[rear];
		return p;
	}

	/**
	 * Gets the item at the given index, where 0 is the front,
	 * 1 is the next item, and so forth.
	 * If no such item exists, returns null. Must not alter the deque!
	 * @param index
	 * @return
	 */
	public Item get(int index) {
		// TODO finished: get
		if (index >= 0 && index < size() && !isEmpty()) {
			index = (front + index ) % maxSize;
			return items[index];
		}
		else return null;
	}

}

import com.sun.xml.internal.bind.v2.TODO;

/**
 * @author Yucheng Wu
 * Created by wyc on 16-4-3.
 *
 * @DS Doubly Linked Lists (Circular Sentinel)
 * @LINK https://docs.google.com/presentation/d/1CqIFP2SPvgJvKKXCmzpRt6e57FYFsjK_Y7vVq0zRFuQ/edit#slide=id.g829fe3f43_0_376
 *
 * Operations are subject to the following rules:
 * @rule1 add and remove operations must not involve any looping or recursion.
 * A single such operation must take "constant time",
 * i.e. execution time should not depend on the size of the Deque.
 * @rule2 get must use iteration, not recursion.
 * @rule3 size must take constant time.
 * @rule4 The amount of memory that your program uses at any given time must
 * be proportional to the number of items. For example, if you add 10,000 items
 * to the Deque, and then remove 9,999 items, the resulting size should be
 * more like a deque with 1 item than 10,000. Do not maintain references to items
 * that are no longer in the Deque.
 */
public class LinkedListDeque<Item> {
	public class SNode {
		public Item item;
		public SNode prev, next;

		public SNode(Item item, SNode prev, SNode next) {
			this.item = item;
			this.prev = prev;
			this.next = next;
		}
	}
	private int size;
	/**
	 * sentinel.next point to the first item of the link.
	 * sentinel.prev point to the last item of the link.
	 * both all them are null indicates the link is empty.
	 */
	private SNode sentinel;
	// SList<Integer> s1 = new SList<Integer>(5);

	/**
	 * Creates an empty linked list deque.
	 */
	public LinkedListDeque() {
		// TODO finished: LinkedListDeque
		sentinel = new SNode(null, null, null);
		sentinel.next = sentinel;
		sentinel.prev = sentinel;
		size = 0;
	}

	/**
	 * Adds an item to the front/back of the Deque.
	 * @link http://blog.csdn.net/cyp331203/article/details/42388065
	 * @param item
	 */
	public void addFirst(Item item) {
		// TODO finished: addFirst
		SNode newNode = new SNode(item, null, null);
		newNode.next = sentinel.next;
		newNode.prev = sentinel;
		sentinel.next.prev = newNode;
		sentinel.next = newNode;
		size += 1;
	}

	public void addLast(Item item) {
		// TODO finished: addLast
		SNode newNode = new SNode(item, null, null);
		newNode.next = sentinel;
		newNode.prev = sentinel.prev;
		sentinel.prev.next = newNode;
		sentinel.prev = newNode;
		size += 1;
	}

	/**
	 * Returns true if deque is empty, false otherwise.
	 * @return
	 */
	public boolean isEmpty() {
		// TODO finished: isEmpty
		return sentinel.next == sentinel;
	}

	/**
	 * Returns the number of items in the Deque.
	 * @return
	 */
	public int size() {
		// TODO finished: size
		return size;
	}

	/**
	 * Prints the items in the Deque from first to last, separated by a space.
	 */
	public void printDeque() {
		// TODO finished: printDeque
		SNode p = sentinel;
		while (p.next != sentinel) {
			System.out.print(p.item + " ");
			p = p.next;
		}
		System.out.println();
	}

	/**
	 * Removes and returns the item at the front of the Deque.
	 * If no such item exists, returns null.
	 * @return
	 */
	public Item removeFirst() {
		// TODO finished: removeFirst
		SNode p = sentinel.next;
		sentinel.next.next.prev = sentinel;
		sentinel.next = sentinel.next.next;
		size -= 1;
		return p.item;
	}

	/**
	 * Removes and returns the item at the back of the Deque.
	 * If no such item exists, returns null.
	 * @return
	 */
	public Item removeLast() {
		// TODO finished: removeLast
		SNode p = sentinel.prev;
		sentinel.prev.prev.next = sentinel;
		sentinel.prev = sentinel.prev.prev;
		size -= 1;
		return p.item;
	}
	
	/**
	 * Gets the item at the given index, where 0 is the front,
	 * 1 is the next item, and so forth.
	 * If no such item exists, returns null. Must not alter the deque!
	 * @param index
	 * @return
	 */
	public Item get(int index) {
		// TODO finished: get
		if (index >= 0 && index < size && !isEmpty()) {
			SNode p = sentinel;
			if (index < size/2) {
				while (index >= 0) {
					p = p.next;
					index--;
				}
			} else {
				index = size - index;
				while (index > 0) {
					p = p.prev;
					index--;
				}
			}
			return p.item;
		}
		return null;
	}

	// search from front
	public SNode getRecursiveHelperF(int index, SNode p) {
		if (index == 0) {
			return p.next;
		}
		return getRecursiveHelperF(index--, p.next);
	}
	// search from end
	public SNode getRecursiveHelperE(int index, SNode p) {
		if (index == 0) {
			return p;
		}
		return getRecursiveHelperE(index--, p.prev);
	}
	/**
	 * Same as get, but uses recursion.
	 * @param index
	 * @return
	 */
	public Item getRecursive(int index) {
		// TODO : getRecursive
		if (index >= 0 && index < size && !isEmpty()) {
			SNode p;
			if (index < size / 2) {
				p = getRecursiveHelperF(index, sentinel);
			} else {
				index = size - index;
				p = getRecursiveHelperE(index, sentinel);
			}
			return p.item;
		}
		return null;
	}
}


/**
 * Created by wyc on 16-4-27.
 */
package editor;

import javafx.scene.text.Text;

import java.util.LinkedList;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Created by wyc on 16-4-27.
 */
public class TextBuffer {

	TextBuffer() {
		currentPos = 0;
		enterText = new StringBuffer();
	}
	/** llText store all the typed letter */
	// llText Everything the user has typed.
//	private LinkedList<Text> llText = new LinkedList<>();
//	private LinkedList<String> typedText = new LinkedList<>(); // convert llText to Sting typedText
//	private LinkedList<Character> enterText = new LinkedList<>();

	private StringBuffer enterText;

	private int currentPos;

	/**
	 * This adds the given character to the string in currentPos
	 * @param c
	 */
	public void addChar(char c) {
		enterText.insert(currentPos, c);
		currentPos += 1;
	}

	/**
	 * Deletes the character at the current position
	 */
	public void deleteChar() {
		int start = currentPos;
		int end = enterText.length();
		if (end < 1 || start < 1) {
			return;
		}
		System.out.println( "start :" +  start + " end : "+end);
		String replaceStr = enterText.substring(start, end);
		enterText.replace(start - 1, end, replaceStr);
		currentPos -= 1;
	}

	/**
	 * Returns the ith character of the enteredText
	 * @param i
	 * @return
	 */
	public char get(int i) {
		return enterText.charAt(i);
	}
	/**
	 * Sets the current position of the TextBuffer
	 * @param x
	 */
	public void setPos(int x) {
		currentPos = x;
	}
	public int getCurrentPos() {
		return currentPos;
	}

	public String getText() { return enterText.toString();}
}

/**
 * Task 1: The ability to show all characters typed so far (instead of just the most recent).
 * Task 2: Display of text in the top left corner of the window (instead of in the middle).
 * Task 3: The ability to delete characters using the backspace key.
 *
 * borrowed heavily from SingleLetterDisplaySimple.java
 */
package editor;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ScrollBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.LinkedList;

/**
 * the code borrowed heavily from
 */
public class Editor extends Application {
	// TODO proj2: 16-4-17 Cursor
	private final Rectangle textBoundingBox;
	private TextBuffer buffer;
	private static final int WINDOW_WIDTH = 500;
	private static final int WINDOW_HEIGHT = 500;
	/**
	 * store the cursor position and size
	 */
	private double cursorHeight, cursorWidth, cursorX, cursorY;

	public Editor() {
		// Create a rectangle to surround the text that gets displayed.  Initialize it with a size
		// of 1
		textBoundingBox = new Rectangle(1, 1);
		buffer = new TextBuffer();
	}

	/**
	 * An EventHandler to handle keys that get pressed.
	 */
	private class KeyEventHandler implements EventHandler<KeyEvent> {
		int textCenterX;
		int textCenterY;

		private static final int STARTING_FONT_SIZE = 20;
		private static final int STARTING_TEXT_POSITION_X = 250;
		private static final int STARTING_TEXT_POSITION_Y = 250;

		/**
		 * The Text to display on the screen.
		 */
		private Text displayText = new Text(STARTING_TEXT_POSITION_X, STARTING_TEXT_POSITION_Y, "");
		private int fontSize = STARTING_FONT_SIZE;

		private String fontName = "Verdana";


		/**
		 * llText store all the typed letter
		 */
		private LinkedList<Text> llText = new LinkedList<>();
		private LinkedList<String> typedText = new LinkedList<>(); // convert llText to Sting typedText


		KeyEventHandler(final Group root, int windowWidth, int windowHeight) {
//			textCenterX = windowWidth / 2;
//			textCenterY = windowHeight / 2;
			// TODO finished PROJ2: Window size and margins 500*500
			textCenterX = 5;
			textCenterY = 0;

			// Initialize some empty text and add it to root so that it will be displayed.
			displayText = new Text(textCenterX, textCenterY, "");
			// Always set the text origin to be VPos.TOP! Setting the origin to be VPos.TOP means
			// that when the text is assigned a y-position, that position corresponds to the
			// highest position across all letters (for example, the top of a letter like "I", as
			// opposed to the top of a letter like "e"), which makes calculating positions much
			// simpler!
			displayText.setTextOrigin(VPos.TOP);
			displayText.setFont(Font.font(fontName, fontSize));
			displayText.setWrappingWidth(WINDOW_WIDTH - 30);

			// All new Nodes need to be added to the root in order to be displayed.
			root.getChildren().add(displayText);
		}

		@Override
		public void handle(KeyEvent keyEvent) {
			/**
			 * Character input is reported by KEY_TYPED events:
			 * KEY_PRESSED and KEY_RELEASED events are not necessarily associated with character input.
			 * For example, pressing the Shift key will cause a KEY_PRESSED event with a VK_SHIFT keyCode,
			 * while pressing the 'a' key will result in a VK_A keyCode. After the 'a' key is released, a KEY_RELEASED
			 * event will be fired with VK_A. Separately, a KEY_TYPED event with a keyChar value of 'A' is generated.
			 */
			if (keyEvent.isShortcutDown()) {
				// TODO proj2:  Shortcut keys
				if (keyEvent.getCode() == KeyCode.S) {
					System.out.println("save file");
				} else if (keyEvent.getCode() == KeyCode.O) {
					System.out.println("open file");
				}
			} else if (keyEvent.getEventType() == KeyEvent.KEY_TYPED) {
				// Use the KEY_TYPED event rather than KEY_PRESSED for letter keys, because with
				// the KEY_TYPED event, javafx handles the "Shift" key and associated
				// capitalization.
				String characterTyped = keyEvent.getCharacter();
				if (characterTyped.length() > 0 && characterTyped.charAt(0) != 8) {
					// Ignore control keys, which have non-zero length, as well as the backspace
					// key, which is represented as a character of value = 8 on Windows.
					// TODO finished: Task 1: The ability to show all characters typed so far
					buffer.addChar(characterTyped.charAt(0));
					double lastX = 0;
					if (!llText.isEmpty()) {
						lastX = llText.getLast().getLayoutBounds().getMaxX();
					}
//					double lastY = llText.getLast().getY();
					Text newText = new Text(characterTyped);
					llText.add(newText);
					newText.setTextOrigin(VPos.TOP);
					llText.getLast().setX(lastX + newText.getLayoutBounds().getWidth());
					typedText.add(newText.getText());
//					System.out.println(" llText.getLast() " + llText.get(1).getX());
					String printText = typedText.toString().replaceAll("[\\[\\]]", "").replaceAll(", ", "");
//					System.out.println(printText);
					displayText.setText(buffer.getText());
					System.out.println("buffer.tostring " + buffer.getText());
					keyEvent.consume();
				}
				centerText();
			} else if (keyEvent.getEventType() == KeyEvent.KEY_PRESSED) {
				// Arrow keys should be processed using the KEY_PRESSED event, because KEY_PRESSED
				// events have a code that we can check (KEY_TYPED events don't have an associated
				// KeyCode).
				KeyCode code = keyEvent.getCode();
				if (code == KeyCode.UP) {
					fontSize += 5;
					displayText.setFont(Font.font(fontName, fontSize));
					centerText();
				} else if (code == KeyCode.DOWN) {
					fontSize = Math.max(0, fontSize - 5);
					displayText.setFont(Font.font(fontName, fontSize));
					centerText();
				} else if (code == KeyCode.BACK_SPACE) {
					// TODO finished: Task 3
					buffer.deleteChar();
					llText.removeLast();
					typedText.removeLast();
					String printText = typedText.toString().replaceAll("[\\[\\]]", "").replaceAll(", ", "");
					displayText.setText(buffer.getText());
					centerText();
				}
			}
		}

		private void centerText() {
			// Figure out the size of the current text.
			double textHeight = displayText.getLayoutBounds().getHeight();
			double textWidth = displayText.getLayoutBounds().getWidth();

//			System.out.println("textHeight :" + textHeight + "	***	" + "textWidth :" + textWidth);
			System.out.println("font size :" + displayText.getFont().getSize());

			// TODO finished: Task 2
			double textLeft = textCenterX;
			double textTop = textCenterY;

			// Re-position the text.
			displayText.setX(textLeft);
			displayText.setY(textTop);

			// Re-size and re-position the bounding box.
			cursorHeight = llText.getLast().getLayoutBounds().getHeight();
			cursorWidth = 1;
			textBoundingBox.setHeight(cursorHeight);
			textBoundingBox.setWidth(cursorWidth);

			System.out.println("textWidth " + " " + llText.getLast().getX());

			Text text = new Text(buffer.getText());

			cursorY = text.getLayoutBounds().getMinY();
			cursorX = text.getLayoutBounds().getWidth() + text.getLayoutBounds().getMinX();

			textBoundingBox.setX(cursorX);
			textBoundingBox.setY(cursorY);

			System.out.println("Bounding box: " + textBoundingBox);

			// Make sure the text appears in front of any other objects you might add.
			displayText.toFront();
		}
	}

	/**
	 * An EventHandler to handle changing the color of the rectangle.
	 */
	private class RectangleBlinkEventHandler implements EventHandler<ActionEvent> {
		private int currentColorIndex = 0;
		private Color[] boxColors = {Color.BLACK, Color.WHITE};
//				{Color.LIGHTPINK, Color.ORANGE, Color.YELLOW,
//						Color.GREEN, Color.LIGHTBLUE, Color.PURPLE};

		RectangleBlinkEventHandler() {
			// Set the color to be the first color in the list.
			changeColor();
		}

		private void changeColor() {
			textBoundingBox.setFill(boxColors[currentColorIndex]);
			currentColorIndex = (currentColorIndex + 1) % boxColors.length;
		}

		@Override
		public void handle(ActionEvent event) {
			changeColor();
		}
	}

	/**
	 * Makes the text bounding box change color periodically.
	 */
	public void makeRectangleColorChange() {
		// Create a Timeline that will call the "handle" function of RectangleBlinkEventHandler
		// every 1 second.
		final Timeline timeline = new Timeline();
		// The rectangle should continue blinking forever.
		timeline.setCycleCount(Timeline.INDEFINITE);
		RectangleBlinkEventHandler cursorChange = new RectangleBlinkEventHandler();
		KeyFrame keyFrame = new KeyFrame(Duration.seconds(0.5), cursorChange);
		timeline.getKeyFrames().add(keyFrame);
		timeline.play();
	}

	@Override
	public void start(Stage primaryStage) {
		// Create a Node that will be the parent of all things displayed on the screen.
		Group root = new Group();
		// The Scene represents the window: its height and width will be the height and width
		// of the window displayed.
		Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT, Color.WHITE);

		// To get information about what keys the user is pressing, create an EventHandler.
		// EventHandler subclasses must override the "handle" function, which will be called
		// by javafx.
		EventHandler<KeyEvent> keyEventHandler =
				new KeyEventHandler(root, WINDOW_WIDTH, WINDOW_HEIGHT);
		// Register the event handler to be called for all KEY_PRESSED and KEY_TYPED events.
		scene.setOnKeyTyped(keyEventHandler);
		scene.setOnKeyPressed(keyEventHandler);

		primaryStage.setTitle("Editor");
		root.getChildren().add(textBoundingBox);
		makeRectangleColorChange();

		// Make a vertical scroll bar on the right side of the screen.
		ScrollBar scrollBar = new ScrollBar();
		scrollBar.setOrientation(Orientation.VERTICAL);
		// Set the height of the scroll bar so that it fills the whole window.
		scrollBar.setPrefHeight(WINDOW_HEIGHT);

		// Add the scroll bar to the scene graph, so that it appears on the screen.
		root.getChildren().add(scrollBar);

		double usableScreenWidth = WINDOW_WIDTH - scrollBar.getLayoutBounds().getWidth();
		System.out.println(scrollBar.getLayoutBounds().getWidth() + "scrollBar");
		scrollBar.setLayoutX(usableScreenWidth);


		// This is boilerplate, necessary to setup the window where things are displayed.
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		// TODO proj2: Command line arguments
		/**
		 * Your Editor program should accept one required command line argument representing the name of the file to edit.
		 */
		boolean debug = false;
		if (args.length < 1 || args.length > 2) {
			System.out.println("Expected usage: <source filename> <\"debug\">[optional]");
			System.exit(1);
		}
		if (args.length == 2) {
			if (args[1] == "debug") {
				debug = true;
			}
		}
		String inputFilename = args[0];

//		try {
//			File inputFile = new File(inputFilename);
//			if(!inputFile.exists()) {
//				System.out.println("file with name " + inputFilename  + " does not exist");
//			}
//			FileReader reader = new FileReader(inputFile);
//		} catch (FileNotFoundException fileNotFoundException) {
//			System.out.println("File not found! Exception was: " + fileNotFoundException);
//		}

		launch(args);
		// TODO: 16-4-17 to get display.Text then write the content to file inputFilename


	}
}

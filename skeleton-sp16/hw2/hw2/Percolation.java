package hw2;

import edu.princeton.cs.algs4.WeightedQuickUnionUF;
//import examples.In;

public class Percolation {
	private final static int BLOCK  = 0;
	private static final int OPEN = 1;
	private int N;
	private int N2; //N*N
//	private int[][] grid;
	private int[] linear;
	private WeightedQuickUnionUF wquuf;
	private WeightedQuickUnionUF auxWquuf;
	private int numberOfOpenSites = 0;
//	private int top, bottom;
	public Percolation(int N){
		if (N <= 0) {
			throw new IllegalArgumentException(Integer.toString(N));
		}
		this.N = N;
		this.N2 = N*N;// may overflow
//		grid = new int[N][N];
		linear = new int[this.N2 + 2];
		wquuf = new WeightedQuickUnionUF(this.N2 + 2);
		auxWquuf = new WeightedQuickUnionUF(this.N2 + 1);
		for (int i = 0; i < this.N2; i++) {
			linear[i] = BLOCK;
		}
//		top = N2;
//		bottom = N2 + 1;
		linear[this.N2] = OPEN;		//virtual top site which connects all the elements in the first row
		linear[this.N2 + 1] = OPEN;	//virtual bottom site which connects all the elements in the last row
		/*for (int i = 0; i < N; i++) {
			wquuf.union(linear[this.N], linear[xyTo1D(0,i)]);
			wquuf.union(linear[this.N + 1], linear[xyTo1D(N - 1, i)]);
		}*/
	}                // create N-by-N grid, with all sites initially blocked

//	public static void main(String[] args){}   // unit testing (not required)

	private int xyTo1D(int row, int col) {
		// verify the legality of the bounds of the given row and col number
		if (row < 0 || row >= N) {
			throw new IndexOutOfBoundsException("row "+ row +" out of bound");
		}

		if (col < 0 || col >= N) {
			throw new IndexOutOfBoundsException("col "+ col +" out of bound");
		}
		return row*N + col;
	}
	public void open(int row, int col) {
		if (!isOpen(row, col)) {
//			grid[row][col] = OPEN;
//			int loc = xyTo1D(row, col);
			int index = xyTo1D(row, col);
			linear[index] = OPEN;
			numberOfOpenSites++;
			/* union with the top virtual site if the element of the first row is opened,
				otherwise, union with the bottom virtual site if it locates in the last row.
			 */
			if (index < N) { //top
				wquuf.union(linear[index], N2);
				auxWquuf.union(linear[index], N2);
			} else if (index >= N && index >= N * (N - 1)) { // bottom
				wquuf.union(linear[index], N2 + 1);
			}

			// union the opened sites
			if (row != N-1 && isOpen(row + 1, col)) { // down
				wquuf.union(linear[index], linear[xyTo1D(row + 1, col)]);
				auxWquuf.union(linear[index], linear[xyTo1D(row + 1, col)]);
			}
			if (row != 0 && isOpen(row - 1, col)) { // up
				wquuf.union(linear[index], linear[xyTo1D(row - 1, col)]);
				auxWquuf.union(linear[index], linear[xyTo1D(row - 1, col)]);
			}
			if (col != 0 && isOpen(row, col - 1)) { // left
				wquuf.union(linear[index], linear[xyTo1D(row, col - 1)]);
				auxWquuf.union(linear[index], linear[xyTo1D(row, col - 1)]);
			}
			if (col != N-1 && isOpen(row, col + 1)) { // right
				wquuf.union(linear[index], linear[xyTo1D(row, col + 1)]);
				auxWquuf.union(linear[index], linear[xyTo1D(row, col + 1)]);
			}
		}
	}      // open the site (row, col) if it is not open already

	public boolean isOpen(int row, int col){
		int index = xyTo1D(row, col);
		if (linear[index] == BLOCK) {
			return false;
		}
		return true;
	}  // is the site (row, col) open?

	public boolean isFull(int row, int col){ // may backwash
		int index = xyTo1D(row, col);
		return auxWquuf.connected(index, N2);
	}  // is the site (row, col) full?

	public int numberOfOpenSites(){
		return numberOfOpenSites;
	}           // number of open sites, should constant time

	public boolean percolates(){
		return wquuf.connected(N2, N2 + 1);
	}              // does the system percolate?
}
package hw2;

import edu.princeton.cs.introcs.StdRandom;
import edu.princeton.cs.introcs.StdStats;

public class PercolationStats {
	private int N2;
	private double[] threshold;
	private int T;
	public PercolationStats(int N, int T){
		this.N2 = N*N;
		this.T = T;
		threshold = new double[N2];
		for (int i = 0; i < T; i++) {
			Percolation grid = new Percolation(N);
			while (!grid.percolates()){
				grid.open(StdRandom.uniform(N), StdRandom.uniform(N));
			}
			threshold[i] = (double)grid.numberOfOpenSites()/N2;
		}
	}   // perform T independent experiments on an N-by-N grid

	public static void main(String[] args){}   // unit testing (not required)

	public double mean(){
		return StdStats.mean(threshold);
	}                   // sample mean of percolation threshold

	public double stddev(){
		return StdStats.stddev(threshold);
	}                  // sample standard deviation of percolation threshold

	public double confidenceLow(){
		return mean() - 1.96*stddev()/Math.sqrt(T);
	}           // low  endpoint of 95% confidence interval

	public double confidenceHigh(){
		return mean() + 1.96*stddev()/Math.sqrt(T);
	}          // high endpoint of 95% confidence interval
}

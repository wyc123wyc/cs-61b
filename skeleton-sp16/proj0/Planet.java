import com.oracle.deploy.update.UpdateCheck;

/**
 * @author Yucheng Wu
 * Created by wyc on 16-3-30.
 */
public class Planet {
    public double xxPos;
    public double yyPos;
    public double xxVel;
    public double yyVel;
    public double mass;
    public String imgFileName;
    private final double G = 6.67e-11;

    public Planet(double xP, double yP, double xV, double yV, double m, String img) {
        xxPos = xP;
        yyPos = yP;
        xxVel = xV;
        yyVel = yV;
        mass = m;
        imgFileName = img;
    }

    public Planet(Planet p) {
        xxPos = p.xxPos;
        yyPos = p.yyPos;
        xxVel = p.xxVel;
        yyVel = p.yyVel;
        mass = p.mass;
        imgFileName = p.imgFileName;
    }

    /**
     * Returns the absolute value of the argument.
     */
    public double abs(double x) {
        if (x < 0) return -x;
        return x;
    }

    /**
     * This method will take in a single Planet and
     * should return a double equal to the distance
     * between the supplied planet and the planet that
     * is doing the calculation.
     */
    public double calcDistance(Planet p) {
        double  x = this.abs(xxPos),
                y = this.abs(yyPos),
                xp= this.abs(p.xxPos),
                yp= this.abs(p.yyPos),
                dx= x - xp,
                dy= y - yp,
                dis = Math.sqrt(Math.pow(dx,2) + Math.pow(dy,2));
        return  dis;
    }

    /**
     * The calcForceExertedBy method takes in a planet,
     * and returns a double describing the force exerted
     * on this planet by the given planet.
     */
    public double calcForceExertedBy(Planet p) {
        double  r = this.calcDistance(p),
                F = (G*this.mass*p.mass)/Math.pow(r,2);
        return  F;
    }

	/**
	 * Describe the force exerted in the X.
     */
    public double calcForceExertedByX(Planet p) {
		double	F = calcForceExertedBy(p),
				r = this.calcDistance(p),
				dx= p.xxPos - this.xxPos,
				Fx= (dx * F) / r;
		return  Fx;
    }

	/**
	 * Describe the force exerted in the Y.
	 */
	public double calcForceExertedByY(Planet p) {
		double	F = calcForceExertedBy(p),
				r = this.calcDistance(p),
				dy= p.yyPos - this.yyPos,
				Fy= (dy * F) / r;
		return  Fy;
	}

	/**
	 * Take in an array of Planets and calculate the
	 * net X and net Y force exerted by all planets
	 * in that array upon the current Planet
	 */
	public double calcNetForceExertedByX(Planet[] p) {
		double netFx = 0;
		for (int i = 0; i < p.length; i++) {
			if (p[i] != this) {
				netFx += calcForceExertedByX(p[i]);
			}
		}
		return netFx;
	}

	public double calcNetForceExertedByY(Planet[] p) {
		double netFy = 0;
		for (int i = 0; i < p.length; i++) {
			if (p[i] != this) {
				netFy += calcForceExertedByY(p[i]);
			}
		}
		return netFy;
	}

	/**
	 * update the planet's position and
	 * velocity instance variables.
	 * @param dt a small period of time dt.
	 * @param fX X-force exerted on the planet.
	 * @param fY Y-force exerted on the planet.
	 */
	public void update(double dt, double fX, double fY) {
		double 	ax = fX/this.mass,
				ay = fY/this.mass;
		this.xxVel = this.xxVel + ax * dt;
		this.yyVel = this.yyVel + ay * dt;
		this.xxPos = this.yyPos + this.xxVel * dt;
		this.yyPos = this.yyPos + this.yyVel * dt;
	}

	/**
	 * Use the StdDraw API mentioned above to draw the
	 * Planet's img at the Planet's position.
	 */
	public void draw() {
		//this.imgFileName = "./images/" + this.imgFileName;
		StdDraw.picture(this.xxPos, this.yyPos, "./images/" + this.imgFileName);
		//StdDraw.show(10);
	}
}

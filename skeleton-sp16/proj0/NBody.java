/**
 * @author Yucheng Wu
 * Created by wyc on 16-3-30.
 */

//package proj0;
//import examples.In;
import com.sun.javaws.exceptions.ExitException;
import org.junit.Test;
import org.omg.CORBA.DoubleHolder;

public class NBody {
	/**
	 * Given a file name, it return a double corresponding
	 * to the radius of the universe in that file.
	 */
	@Test
	public static double readRadius(String filename) {
		In in = new In(filename);
		//In in = new In("./data/planets.txt");
		int		N = 0;
		double	R = -1;
		if (!in.isEmpty()) {
			N = in.readInt();	// number of planets
			R = in.readDouble();	// radius of the universe
		}
		return R;
	}

	/**
	 * Given a file name, it return an array of Planets
	 * corresponding to the planets in the file
	 * @param filename
	 * @return an array of Planets
	 */
	public static Planet[] readPlanets(String filename) {
		In in = new In(filename);
		int		N;
		double	R = -1;
		Planet[] planets = null;
		if (!in.isEmpty()) {
			N = in.readInt();
			R = in.readDouble();
			planets = new Planet[N];
			for (int i = 0; i < N; i++) {
				planets[i] = new Planet(
						in.readDouble(),	//planets[i].xxPos
						in.readDouble(),	//planets[i].yyPos
						in.readDouble(),	//planets[i].xxVel
						in.readDouble(),	//planets[i].yyVel
						in.readDouble(),	//planets[i].mass
						in.readString());	//planets[i].imgFileName
			}
		}
		return planets;
	}

	/**
	 * Drawing All of the Planets
	 * @param planets
	 */
	public static void draw(Planet[] planets, double R, String starfield) {

		StdDraw.clear();
		StdDraw.picture(0, 0, starfield);


		for (int i = 0; i < planets.length; i++) {
			//planets[i].draw();
			StdDraw.picture(planets[i].xxPos, planets[i].yyPos, "./images/" + planets[i].imgFileName);
		}
		StdDraw.show(1);
	}

	public static void animation(Planet[] planets, double R, String starfield, double T, double dt) {
		int N = planets.length;
		double[] xForces = new double[N],
				 yForces = new double[N];
		for (double i = 0; i < T; i+=dt) {
			for (int j = 0; j < N; j++) {
				//System.out.println(planets[j].imgFileName +": xxPos="+ planets[j].xxPos +" yyPos="+ planets[j].yyPos);
				xForces[j] = planets[j].calcNetForceExertedByX(planets);
				yForces[j] = planets[j].calcNetForceExertedByY(planets);
				planets[j].update(dt, xForces[j], yForces[j]);
				//System.out.println(planets[j].imgFileName +": xxPos="+ planets[j].xxPos +" yyPos="+ planets[j].yyPos);
			}
			//System.out.println("end " + i + "\n");
			draw(planets, R, starfield);
		}
	}
	public static void main(String[] args) {
		if (args.length != 3) {
			System.out.println("please input 3 command line arguments, T、dt and filename respectively!");
			return;
		}
		double T = Double.parseDouble(args[0]);
		double dt= Double.parseDouble(args[1]);
		String filenmae = args[2];

		double R = readRadius(filenmae);
		Planet[] planets = readPlanets(filenmae);

		String starfield = "./images/starfield.jpg";
		//draw(planets, R, starfield);
		StdDraw.setScale(-2*R, 2*R);
		animation(planets, R, starfield, T, dt);
	}
}

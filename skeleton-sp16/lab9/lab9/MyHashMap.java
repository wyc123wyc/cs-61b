package lab9;

import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;
//import java.util.*;

/**
 * Created by wyc on 16-8-1.
 */
public class MyHashMap<K, V> implements Map61B<K, V> {

	private int FACTOR = 2;
	private double loadFactor;
	private int size;	// hash table size
	private int pairs = 0; // number of key-value pairs
	private MyHashEntry<K, V>[] hashEntries;
//	private MyHashEntry<K, V>[] hashEntry;
	public MyHashMap(){
		this(127, 0.75);
	}
	public MyHashMap(int initialSize){
		this(initialSize, 0.75);
//		this.size = initialSize;
	}
	public MyHashMap(int initialSize, double loadFactor){
		this.size = initialSize;
		this.loadFactor = loadFactor;
		this.hashEntries = (MyHashEntry<K, V>[]) new MyHashEntry[initialSize];
		for (int i = 0; i < initialSize; i++) {
			hashEntries[i] = new MyHashEntry<>();
		}
	}

	/** Removes all of the mappings from this map. */
	public void clear(){
		for (int i = 0; i < size; i++) {
			hashEntries[i].clear();
		}
		pairs = 0;
	}

	/* Returns true if this map contains a mapping for the specified key. */
	public boolean containsKey(K key){
		return get(key) != null;
	}

	/* Returns the value to which the specified key is mapped, or null if this
	 * map contains no mapping for the key.
	 */
	public V get(K key){
		if (key == null) {
			throw new NullPointerException("argument to get() is null");
		}
		int i = hash(key);
		return hashEntries[i].get(key);
	}
	// hash value between 0 and size-1(hash table size minus 1)
	private int hash(K key) {
		return (key.hashCode() & 0x7fffffff) % size;
	}

	/* Returns the number of key-value mappings in this map. */
	public int size(){
		return pairs;
	}

	/* Associates the specified value with the specified key in this map. */
	public void put(K key, V value){
		if (key == null) {
			throw new NullPointerException("first argument to put() is null");
		}
		if (value == null) {
			remove(key);
			return;
		}

		if ( (double)pairs/size > loadFactor) resize(FACTOR * size);
		int i = hash(key);
		if (!hashEntries[i].containsKey(key) ) {
			pairs++;
		}
		hashEntries[i].put(key, value);
	}
	// resize the hash table to have the given number of chains,
	// rehashing all of the keys
	private void resize(int chains) {
		MyHashMap<K, V> temp = new MyHashMap<>(chains);
		Set<K> keySet = temp.keySet();
		for (K key: keySet) {
			temp.put(key, get(key));
		}
		this.size  = temp.size;
		this.pairs  = temp.pairs;
		this.hashEntries = temp.hashEntries;
	}

	/* Returns a Set view of the keys contained in this map. */
	public Set<K> keySet(){
		HashSet<K> arr = new HashSet<K>();
		for (int i = 0; i < size; i++) {
			arr.addAll(hashEntries[i].keySet());
		}
		return arr;
	}

	public Iterator<K> iterator(){
		return new Iterator<K>() {
			@Override
			public boolean hasNext() {return false;}

			@Override
			public K next() {
				return null;
			}
		};
	}

	/* Removes the mapping for the specified key from this map if present.
	 * Not required for Lab 8. If you don't implement this, throw an
	 * UnsupportedOperationException. */
	public V remove(K key){
		throw new UnsupportedOperationException("remove operator is not supported now.");
//		return null;
	}

	/* Removes the entry for the specified key only if it is currently mapped to
	 * the specified value. Not required for Lab 8. If you don't implement this,
	 * throw an UnsupportedOperationException.*/
	public V remove(K key, V value){
		throw new UnsupportedOperationException("remove operator is not supported now.");
	}

	private class MyHashEntry<K, V> implements Iterable<K> {
		private int size;		// number of key-value pairs
		private Node header;	// the linked list of key-value pairs

		MyHashEntry(){
//			header = new Node(null, null, null);
			size = 0;
		}

		/**
		 * remove all of the key-value pairs from the symbol table.
		 */
		public void clear(){
			Node tmp = header.next;
			Node tmpNext;
			while (tmp != null) {
				tmpNext = tmp.next;
				tmp.key = null;
				tmp.val = null;
				tmp.next = null;
				tmp = tmpNext;
			}
			size = 0;
		}

		/**
		 * returns the number of key-value pairs in the symbol table
		 * @return
		 */
		public int size() {return size;}

		/**
		 * return the value associated with the given key
		 * @param key
		 * @return
		 */
		public V get(K key){
			for (Node i = header; i != null; i = i.next) {
				if (key.equals(i.key)) {
					return i.val;
				}
			}
			return null;
		}

		/**
		 * inserts the key-value pairs into the symbol table, overwriting the old value
		 * with the new value if the key is already in the symbol table.
		 * if the value is <tt>null<tt/>, this effectively deletes the key from the symbol table.
		 * @param key
		 * @param val
		 */
		public void put(K key, V val){
			if (val == null) {
				remove(key);
				return;
			}
			if (get(key) != null) {
				return;
			}
			header = new Node(key, val, header);
			size++;
		}

		/**
		 * Does this symbol table contains the given key?
		 * @param key
		 * @return true if this symbol table contains the given key, otherwise false;
		 */
		public boolean containsKey(K key){ return get(key) != null;}

		/**
		 *
		 * @return all keys in the symbol table as an <tt>Set</tt>
		 */
		public Set<K> keySet(){
			HashSet<K> hs = new HashSet<K>();
//			for (K keys: this) { // <>this</> equivalently create a new object whose value is null.
//				hs.add(keys);
//			}
			for (Node i = header; i != null; i = i.next) {
				hs.add(i.key);
			}
			return hs;
		}
		public V remove(K key) { throw new UnsupportedOperationException("remove operator is not supported now.");}
		public V remove(K key, V val) { throw new UnsupportedOperationException("remove operator is not supported now.");}

		@Override
		public Iterator<K> iterator(){
			return new KeyIterator();
		}
		private class KeyIterator implements Iterator<K> {
			private Node nxt;

			KeyIterator() {
				this.nxt = header;
			}
			@Override
			public boolean hasNext() {
				return nxt != null;
			}

			@Override
			public K next() {
				if(nxt.next!=null){
					nxt = nxt.next;
					return nxt.key;
				}
				return nxt.key;
			}
		}

		private class Node{
			private K key;
			private V val;
			private Node next;

			public Node(K key, V val, Node next) {
				this.key = key;
				this.val = val;
				this.next = next;
			}
		}
	}

	public static void main(String[] args) {
		MyHashMap<String, Integer> map = new MyHashMap<>();
		map.put("w1",1);map.put("wy1",2);map.put("wyc1",3);
		map.put("w2",1);map.put("wy2",2);map.put("wyc2",3);
		map.put("w3",1);map.put("wy3",2);map.put("wyc3",3);
 		System.out.println(map.get("wy1"));
		HashSet<String> keys = (HashSet<String>)map.keySet();
		System.out.println(map.get("wyc3"));
		System.out.printf(keys.toString() + keys.size());
	}
}

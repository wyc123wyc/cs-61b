### 提交记录

* 以lab\*, hw\*开头的文件放在skeleton-sp16/目录下
* 以lec\*开头的文件放在lectureCode-sp16/目录下
* 以dis\*开头的文件放在disCode-sp16/目录下
* 自己修改的部分在源文件有@author wyc标识

| 项目                           | 日期          |备注 |
| --------                      | -----         |-----|
| Hash Map		| 2016.8.2		| lab9 modified, write **myHashEntry**, `iteratro` `nested class`|
| Hash Map						| 2016.8.1		| lab9|
| dis9 Hashing					| 2016.7.31		| |
| HW2							| 2016.7.31		| implement two calsses **Percolation.java** and **PercolationStats.java**|
| Algs 424-431				| 2016.7.29		| Balanced BSTs, lec22 |
| BSTMap Test | 2016.7.29 | <a href="http://datastructur.es/sp16/materials/lectures/lec22/lec22.html" target="_blank">lab8(2)</a> |
| Asymptotics dis				| 2016.7.28		||
| Lab 8: BSTs and Asymptotics(without extra)	|2016.7.26| |
|||Create a class **BSTMap** |
| Algs 396-406					| 2016.7.26		| |
| Algs 216-233					| 2016.7.26		| |
| Algs 170-198					| 2016.7.25		| |
| Asymptotics III				| 2016.7.25		| |
| Asymptotic analysis			| 2016.7.24		| |
| Asymptotics II				| 2016.7.24		| |
| Asymptotics I					| 2016.7.24		| |
| hw1/synthesizer/				| 2016.04.14	| hw1 finished |
| lec14/DIY/MapHelper.java		| 2016.04.14	| More Generics |
| proj2/editor/Editor.java		| 2016.04.14	| Lab 5: Project 2 (getting started) |
| lec13/DIY/					| 2016.04.13	| Generics, Autoboxing |
| lec12/DIY/					| 2016.04.13	| package exercise, creating package david.wu.animal|
| proj1c/Deque.java				| 2016.04.10	| proj1c finished, creating 5 files, including Deque,LinkedListDeque,OffByN,OffByOne and Palindrome |
| proj1c/LinkedListDeque.java	|				||
| proj1c/OffByN.java			|				||
| proj1c/OffByOne.java			|				||
| proj1c/Palindrome.java		|				||
| proj1b/TestArrayDeque1B.java  | 2016.04.09	| proj1b finished , creating Test files |
| proj1b/TestLinkedListDeque1B.java| 			| TestArrayDeque1B.java TestLinkedListDeque1B.java |
| proj1/LinkedListDeque.java	| 2016.04.04	| proj1a finished|
| proj1/LinkedListDequeTest.java|				| Using circular sentinel topology|
| proj1/ArrayDeque.java			|				| and generic data structures|
| proj1/ArrayDequeTest.java		|				| |
| lab3/IntList/IntListTest.java	| 2016.04.03	| lab3 unit testing|
| lab3/Intlist/IntList.java		| 2016.04.02	| lab3 unit testing copy **IntList** from lab2, adding two reverse methods|
| lab3/Arithmetic/ArithmeticTest.java|2016.04.03| testing |
| dis3/SList.java              	| 2016.04.02    | copy **Slist** from lec4 and add reverse methonds|
| proj0/NBody.java              | 2016.03.31    | create **Nbody**|  
| proj0/Planet.java             | 2016.03.31    | create **Planet**|
| lec6/DIY/AList.java           | 2016.03.31    | create **Alist**|
| lec4/DIY/SList.java           | 2016.03.29    | create **Slist**|  
| lab2/Intlist/IntList.java     | 2016.03.29    | add 2 menthods  | 

<!--
[PPT中练习](./lectureCode-sp16/README.md)

[实验和作业](./skeleton-sp16/README.md)

## 课程主页 

主站：http://www-inst.eecs.berkeley.edu/~cs61b/archives.html

## github主页 https://github.com/Berkeley-CS61B ### [获取代码框架](http://cs61b.ug/sp16/materials/lab/lab1/lab1.html#d-git-and-remote-repositories) ###  Add the skeleton remote enter the following command to add the skelteon remote.  `$ git remote add skeleton https://github.com/Berkeley-CS61B/skeleton-sp16.git`

### Listing the remotes 
Listing the remotes should now show both the origin and skeleton remotes.

`$ git remote -v`

### Working on the Skeleton
You must now pull from the skeleton remote in order to get the starter code . 
You will also do this when new projects and assignments are released. 
To do this, use the spookiest command in the whole git toolbox:

`$ git pull skeleton master`

more detail: http://cs61b.ug/sp16/materials/lab/lab1/lab1.html#d-git-and-remote-repositories
-->
